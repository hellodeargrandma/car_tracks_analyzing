# -*- coding:utf-8 -*-

import numpy as np
import pandas as pd

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle

from typing import List, Optional


PandasDFList = List[pd.DataFrame]



class TracksIO:
    def __init__(self, filename:str):
        self.src_file = filename


    def read(self) -> PandasDFList:
        offset = 0
        int32_be = np.dtype('>i4')
        n_tracklets = np.fromfile(self.src_file, dtype=int32_be, count=1,
                                  offset=offset)[0]
        offset += int32_be.itemsize

        tracklets = []
        for tr in range(n_tracklets):
            tr_id = np.fromfile(self.src_file, dtype=int32_be, count=1,
                                offset=offset)[0]
            offset += int32_be.itemsize

            # tracked path
            path_size = np.fromfile(self.src_file, dtype=int32_be, count=1,
                                    offset=offset)[0]
            offset += int32_be.itemsize

            path = np.fromfile(self.src_file, int32_be, count=path_size * 5,
                               offset=offset)
            offset += int32_be.itemsize * path_size * 5

            path = path.reshape(path_size, 5)
            pd_path = pd.DataFrame.from_records(path, columns=[
                'x', 'y', 'w', 'h', 'frame_no'])

            # backward predictions
            bwd_size = np.fromfile(self.src_file, dtype=int32_be, count=1,
                                   offset=offset)[0]
            offset += int32_be.itemsize

            bwd_path = np.fromfile(self.src_file, int32_be, count=bwd_size * 4,
                                   offset=offset)
            offset += int32_be.itemsize * bwd_size * 4

            bwd_path = bwd_path.reshape(bwd_size, 4)
            pd_bwd_path = pd.DataFrame.from_records(bwd_path, columns=[
                'x', 'y', 'w', 'h'])

            # forward preditions
            fwd_size = np.fromfile(self.src_file, dtype=int32_be, count=1,
                                   offset=offset)[0]
            offset += int32_be.itemsize

            fwd_path = np.fromfile(self.src_file, int32_be, count=fwd_size * 4,
                                   offset=offset)
            offset += int32_be.itemsize * fwd_size * 4

            fwd_path = fwd_path.reshape(fwd_size, 4)
            pd_fwd_path = pd.DataFrame.from_records(fwd_path, columns=[
                'x', 'y', 'w', 'h'])

            tracklets.append({'path': pd_path,
                              'fwd_path': pd_fwd_path,
                              'bwd_path': pd_bwd_path})

        return tracklets

    def plot(self, tracks, n=-1, plot_bboxes:bool=False, step:int=1,
                max_len:Optional[int]=None):
        if n == -1:
            n = len(tracks)

        fig, ax = plt.subplots(1)

        for tr in tracks[:n]:
            path = tr['path']
            #FIXME: threshold tracks by length smarter or make a parameter for it.

            path = path[:max_len]

            track = (path.iloc[::step, 0:2] +
                     path.iloc[::step, 2:4].rename(columns={'w':'x', 'h':'y'}) / 2)
            #print starting points
            ax.plot(track['x'][0], track['y'][0], '+')

            #print averaged tracks (noise reduce)
            # wnd_size == 1 does nothing
            wnd_size = 1
            ax.plot(track['x'].rolling(window=wnd_size).mean().iloc[5:].values,
                    track['y'].rolling(window=wnd_size).mean().iloc[5:].values,
                    'o-', alpha=0.3)

            if plot_bboxes is True:
                bboxes = []
                for _, row in path.iloc[::step].iterrows():
                    bboxes.append(Rectangle((row['x'], row['y']),
                                            row['w'], row['h'], fill=False))

                pc = PatchCollection(bboxes, facecolor='none', alpha=1, edgecolor='b')
                ax.add_collection(pc)

        ax.set_xlabel("X, px")
        ax.set_ylabel("Y, px")


    # FIXME: checkpoint is not a good term for it here
    def plot_checkpoint (self, checkpoint):
        pass


