# -*- coding:utf8 -*-

import matplotlib.pyplot as plt

import pandas as pd

from car_tracks_analyzer.tracks_io import TracksIO
from car_tracks_analyzer.extract_tracks import extract_tracks
from car_tracks_analyzer.analyze import TracksAnalyzer

from typing import Optional


class Tracks:
    def __init__(self):
        self.tracks = None
        self.tracks_io = None
        self.analyzer = TracksAnalyzer()


    def load(self, fname:str):
        if (self.tracks is not None or
            self.tracks_io is not None):
            #TODO: clear data from old tracks
            pass

        self.tracks_io = TracksIO(fname)
        self.tracks = self.tracks_io.read()


    def extract_tracks(self, start_area:pd.DataFrame, end_area:pd.DataFrame):
        self.tracks = extract_tracks(self.tracks, start_area, end_area)


    def plot(self, n_tracks:int, plot_bboxes:bool, intrack_step:int,
               max_len:Optional[int]=None):
        self.tracks_io.plot(self.tracks, n_tracks, plot_bboxes,
                               intrack_step, max_len)


    def save_plot(self, fname:str):
        if self.tracks is None:
            raise Exception("No tracks loaded.")

        plt.savefig(fname)

    def show_plot(self):
        plt.show()


    def add_checkpoints(self):
        self.checkpoints = self.analyzer.get_checkpoints(self.tracks)


    def plot_checkpoints(self):
        for chkpoint in self.checkpoints:
            self.tracks_io.plot_checkpoint(checkpoint)

