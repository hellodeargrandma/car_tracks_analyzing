#!/usr/bin/env python3

import argparse

import numpy as np
import pandas as pd

from car_tracks_analyzer.tracks import Tracks
from car_tracks_analyzer.extract_tracks import convert_areas



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--in-file", type=str, help="File to process.",
                        required=True)
    parser.add_argument("--out-file", type=str, help="Were to save png tracks.",
                        required=False)
    parser.add_argument("--max-len", type=int, default=100, metavar='N',
                        help="""Maximum track len.""")
    parser.add_argument("--n-tracks", type=int, default=-1, metavar='N',
                        help="""Count of tracks to process. First N tracks
                        will be processed.""")
    parser.add_argument("--intrack-step", type=int, default=1, metavar='N',
                        help="""Step used while plotting tracks. Every N point
                        in track will be processed.""")
    parser.add_argument("--plot-tracks", type=bool, default=True,
                        help="If you want to plot tracks.")
    parser.add_argument("--plot-bboxes", type=bool, default=False,
                        help="""If you want to plot cars bounding boxes on 2D
                        plot.""")
    parser.add_argument("--start-area", type=str, default="0,0,-1,-1",
                        help="The area where tracks should start.")
    parser.add_argument("--end-area", type=str, default="0,0,-1,-1",
                        help="The area where tracks should end.")
    args = parser.parse_args()

    start_area, end_area = convert_areas([args.start_area, args.end_area])

    tracks = Tracks()
    tracks.load(args.in_file)

    tracks.extract_tracks(start_area, end_area)

    if args.plot_tracks:
        tracks.plot(args.n_tracks, args.plot_bboxes,
                       args.intrack_step, max_len=args.max_len)

    if args.out_file:
        tracks.save_plot(args.out_file)
        return

    tracks.add_checkpoints()
    tracks.plot_checkpoints()
    tracks.show_plot()


if __name__ == "__main__":
    main()
