#!/usr/bin/env python3

import argparse
from typing import Tuple

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from car_tracks_analyzer.tracks_io import TracksIO


def extract_tracks(tracks:np.ndarray, start_area:pd.DataFrame,
                   end_area:pd.DataFrame) -> np.ndarray:
    """ Extracts tracks that starts in start_area and ends in end_area.

    Parameters
    ==========

    start_area
        Four coordinates of area: x,y of the nearest to origin corner and
        x,y of the corner diagonal to the first one.
    end_area
        Four coordinates of area: same as for start_area.

    Returns
    =======
        The list of tracks.
    """

    # FIXME: ACHTUNG!!!! Stupid code here. Use pandas or numpy magic instead!
    res = np.array([])
    for track in tracks:
        track_coords = track['path'].iloc[0][['x','y']]
        if ((start_area.iloc[1]['x'] == -1 and
             start_area.iloc[1]['y'] == -1)
            or
            (start_area.iloc[0]['x'] < track_coords['x'] and
             start_area.iloc[0]['y'] < track_coords['y'] and
             track_coords['x'] < start_area.iloc[1]['x'] and
             track_coords['y'] < start_area.iloc[1]['y'])
            or
            (end_area.iloc[0]['x'] < track_coords['x'] and
             end_area.iloc[0]['y'] < track_coords['y'] and
             track_coords['x'] < end_area.iloc[1]['x'] and
             track_coords['y'] < end_area.iloc[1]['y'])):

            res = np.append(res, track)
    return res


def convert_areas(areas: np.ndarray) -> Tuple:
    a = np.array([int(c) for c in areas[0].split(',')])
    a1= pd.DataFrame(data={'x':a[[0, 2]], 'y':a[[1, 3]]})

    a = np.array([int(c) for c in areas[1].split(',')])
    a2= pd.DataFrame(data={'x':a[[0, 2]], 'y':a[[1, 3]]})

    return (a1, a2)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--in-file", type=str, help="File to process.",
                        required=True)
    parser.add_argument("--out-file", type=str, help="Were to save png tracks.",
                        required=False)
    parser.add_argument("--start-area", type=str, default="0,0,-1,-1",
        help="The area where tracks should start.")
    parser.add_argument("--end-area", type=str, default="0,0,-1,-1",
        help="The area where tracks should end.")
    args = parser.parse_args()

    tracks_io = TracksIO(args.in_file)
    tracks = tracks_io.read()

    start_area, end_area = convert_areas([args.start_area, args.end_area])
    print(start_area)
    print(end_area)

    tracks = extract_tracks(np.asarray(tracks), start_area, end_area)
    if args.out_file is not None:
        tracks_io.save(tracks, args.out_file)
        return

    tracks_io.plot_2d(tracks)
    plt.show()


if __name__ == "__main__":
    main()
