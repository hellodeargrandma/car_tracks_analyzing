#!/usr/bin/env python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

install_requires = ["pandas", "matplotlib", "numpy"]
entry_points = {
        "console_scripts": [
            "show_tracks = car_tracks_analyzer.show_tracks:main"
        ]
}

setuptools.setup(
    name="car_tracks_analyzer",
    version="0.0.1",
    author="Roman Kovtyukh",
    author_email="HelloDearGrandma@gmail.com",
    description="Car tracks analyzer.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.8,<4",
    install_requires = install_requires,
    entry_points=entry_points,
)

